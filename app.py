import socket
socket.getaddrinfo('localhost', 3306)
import mysql.connector as mariadb
from flask import Flask,request,jsonify
import json

app = Flask(__name__)


@app.route('/insert-record',methods=['GET', 'POST'])
def insertrecord():
	data = request.get_data(as_text=True)
	data = json.loads(data)
	mariadb_connection = mariadb.connect(user='root', password='', database='oneticket',)
	cursor = mariadb_connection.cursor()
	name=data['name']
	email=data['email']
	phone=data['phone']
	dob=data['dob']
	credit='1000'
	password=data['password']
	gender=data['gender']
	cursor.execute("SELECT COUNT(*) FROM users WHERE email=%s",(email,))
	temp=cursor.fetchone()[0]
	print(temp)
	if(temp!=0):
		cursor.close()
		return jsonify({"status":"exist"})
	else:
		cursor.execute("INSERT INTO users(email, name, phone, dob, credit, password, gender) VALUES (%s,%s,%s,%s,%s,%s,%s)",(email,name,phone,dob,credit,password,gender,))
		mariadb_connection.commit()
		cursor.close()
		return jsonify({"status":"done"})
	return jsonify({"status":"error"})
# @app.route('/get-record',methods=['GET', 'POST'])
# def getrecord():


# generates ticket(add record in central database and sends back the ticket ID)
@app.route('/generate-ticket',methods=['GET', 'POST'])
def generateticket():
	data = request.get_data(as_text=True)
	data = json.loads(data)
	date = data['date']
	name = data['name']
	userId = data['userId']
	status="unused"
	mariadb_connection = mariadb.connect(user='root', password='', database='oneticket', )
	cursor = mariadb_connection.cursor()
	cursor.execute("SELECT oneticket.tickets.ticketID FROM tickets ORDER BY ticketID DESC LIMIT 1")
	ticketId = 1 + cursor.fetchone()[0]
	data['tickeId']=ticketId
	cursor.execute("INSERT INTO tickets(ticketID, userId, name, date,status) VALUES (%s,%s,%s,%s,%s)",(ticketId,userId,name,date,status,))
	mariadb_connection.commit()
	item_index=0
	for item in data['tickets']:
		source=item['source']
		destination=item['destination']
		number=item['number']
		type=item['type']
		
		if(type=="metro"):
			cursor.execute("SELECT oneticket.metro_ticket.id FROM metro_ticket ORDER BY id DESC LIMIT 1")
			id = 1 + cursor.fetchone()[0]
			cursor.execute("INSERT INTO metro_ticket(ticketId, id, date, name, source, destination, number, status) VALUES (%s,%s,%s,%s,%s,%s,%s,%s)",(ticketId, id, date, name, source, destination, number, status,))
			mariadb_connection.commit()
			data['tickets'][item_index]['id']=id
		if(type == "bus"):
			cursor.execute("SELECT oneticket.bus_ticket.id FROM bus_ticket ORDER BY id DESC LIMIT 1")
			id = 1 + cursor.fetchone()[0]
			cursor.execute("INSERT INTO bus_ticket(ticketId, id, date, name, source, destination, number, status) VALUES (%s,%s,%s,%s,%s,%s,%s,%s)",(ticketId, id, date, name, source, destination, number, status,))
			mariadb_connection.commit()
			data['tickets'][item_index]['id'] = id
		item_index+=1
	
	cursor.close()
	return jsonify(data)
	
if __name__ == '__main__':
	app.run()


